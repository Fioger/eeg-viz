import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy import signal
from scipy.stats import zscore
import mne
from mne.preprocessing.ica import ICA

# coding: utf-8

# In[1]:

from scipy.io import loadmat
from scipy import signal
from eegtools.io import load_edf

# In[2]:

#m = mne.io.read_raw_edf('s5d2_final.edf', stim_channel=None, preload=True)
"""
ica = ICA(n_components=64, n_pca_components=None, max_pca_components=None, max_iter=100, noise_cov=None, random_state=5)
picks = mne.pick_types(m.info, meg=False, eeg=True, eog=False, stim=False)
ica.fit(m, picks=picks, decim=3, reject=dict(mag=4e-12, grad=4000e-13))
ica.detect_artifacts(m, eog_ch='ROC', eog_criterion=0.5)
clean_raw = ica.apply(m, exclude=ica.exclude)
clean_raw.filter(1, 100)
scipy.io.savemat('ICA_filtered.mat',mdict={'arr':clean_raw._data})
"""
#m.filter(1,100)
#scipy.io.savemat('raw_eeg.mat',mdict={'arr':m._data})


# In[3]:

#matlab load
#matrix = m['s5d2nap']

#edf load
matrix = m[0]

#lowpass
b1, a1 = signal.butter(2, 0.1, 'low', analog=True)
w1, h1 = signal.freqs(b1, a1)

#highpass
b2, a2 = signal.butter(2, 0.5, 'high', analog=True)
w2, h2 = signal.freqs(b2, a2)

low = 0.3
high = 30
fs = 1000

lowcut = low/(0.5*fs)
highcut = high/(0.5*fs)


#bandpass
b3, a3 = signal.butter(2, [lowcut,highcut], 'band')
w3, h3 = signal.freqs(b3, a3)

#create matrix
new_matrix = []

for index, row in (enumerate(matrix)):
	if(index!=63 and index!=62 and index!=61):
		print index
		new_matrix.append(row)

# print len(new_matrix[0])
print len(new_matrix)
print len(new_matrix[0])

trans = np.transpose(new_matrix)
print len(trans)
iterate = (len(new_matrix[0]) / 30000)+1
print iterate

for i in range(iterate):
	for node in range(61):
		if((i + 1) * 30000>len(new_matrix[0])):
			print "check"
			new_matrix[node][i * 30000:] = signal.detrend(np.array(new_matrix[node][i * 30000:]))
		else:
			new_matrix[node][i * 30000:(i + 1) * 30000] = signal.detrend(np.array(new_matrix[node][i * 30000:(i + 1) * 30000]))

print "So"
#detrended = np.transpose(trans)
#detrend and make the mean 0 for the data
#eeg = signal.detrend(np.array(new_matrix),type='constant')


#output the raw data


#normal print
#]mat = []
#for row in range(len(new_matrix)):
#	mat.append(eeg[row])

#s = pd.DataFrame(mat).transpose()

#lowpass print
#mat1 = []
#for row in range(len(new_matrix)):
#	mat1.append(signal.lfilter(b1, a1, eeg[row]))

#s1 = pd.DataFrame(mat1).transpose()

#highpass print
#mat2 = []
#for row in range(len(new_matrix)):
#	mat2.append(signal.lfilter(b2, a2, eeg[row]))
#s2 = pd.DataFrame(mat2).transpose()

#highpass print
#mat2 = []
#for row in range(len(new_matrix)):
#	mat2.append(signal.lfilter(b2, a2, eeg[row]))
#s2 = pd.DataFrame(mat2).transpose()

#bandpass

mat3 = []
for row in range(len(new_matrix)):
	mat3.append(signal.lfilter(b3, a3, new_matrix[row]))

"""
"""
#clipped
#stemp = pd.DataFrame(mat3).clip(-300,300)
stemp = pd.DataFrame(mat3)
"""
"""

#s3 = pd.DataFrame((stemp))
#s3 = pd.DataFrame(eeg)
print "DataFrame"
#s3 = pd.DataFrame(new_matrix)

print "array"
band = np.array(mat3)

print "dumping"
band.dump('ICA_30sec_.dumps')
print "dumped"

#print eeg.shape
#eeg.dump('ICA_data_raw.dumps')

# In[ ]:
#s.plot(legend=False)
#s1.plot(legend=False)
#s2.plot(legend=False)
print "\n Huh?"
s3.plot(legend=False)
print "Did that work?"
# cumsum() adds the value of each channel and displays the sum
# s = s.cumsum()
# s.plot()


# fig = plt.figure()
# fig.savefig('matrix.svg')


#plt.figure(2);

# plt.plot(eeg[:8,:30000].T + 8000*np.arange(7,-1,-1));

# plt.plot(np.zeros((30000,8)) + 8000*np.arange(7,-1,-1),'--',color='gray');

#plt.yticks([]);

# plt.legend(first['channels']);

#plt.axis('tight');


#plt.show()