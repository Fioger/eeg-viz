from scipy.stats import pearsonr
from scipy.io import loadmat
from eegtools.io import load_edf
import json as simplejson
import numpy as np


file_name = raw_input("Please enter the file you want to perform Pearson Correlation on: ")

if file_name.endswith('.mat'):
	data = loadmat(file_name)['arr']
elif file_name.endswith('.edf'):
	data = load_edf(file_name)
else:
	print "Invalid file format."
	quit()

option = raw_input("Please enter 1 if it is a raw file, 2 if it is an ICA file: ")

if option == '1':
	new_file_name = 'Pearson_raw.json'
elif option == '2':
	new_file_name = 'Pearson_ica.json'
else:
	print "You did not choose option 1 or 2."
	quit()

length_of_intervals = 30000
channels = 61
edges = 1830
total_interval = len(data[0])/length_of_intervals + 1
channels_data = [[None for j in range(edges)] for i in range(total_interval)] 


def calculate_pearson(interval):
    index_counter = 0
    x = length_of_intervals * interval
    y = x + length_of_intervals
    for start in range(channels):
        for index in range(start + 1, channels):
            if (pearsonr(data[start][x:y], data[index][x:y])[1] >= 0.05):
                channels_data[interval][index_counter] = 0
            else:
                channels_data[interval][index_counter] = pearsonr(data[start][x:y], data[index][x:y])[0]                
            index_counter += 1

for i in range(total_interval):
    print 'Calculating Interval %s' % i
    calculate_pearson(i)

with open(new_file_name, 'w+') as f:
    p = [[round(float(column), 3) for column in row] for row in channels_data]
    f.write(simplejson.dumps({'name': 'subject', 'data': p}))

print "Done Pearson Correlation, file saved as %s" %new_file_name