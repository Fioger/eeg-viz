import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.io as sci
import numpy as np
import pandas as pd
from scipy import signal

ICA = sci.loadmat('edf_ICA_channels.mat')['arr']

fig1 = plt.figure()
fig2 = plt.figure()
fig3 = plt.figure()
fig4 = plt.figure()
figs = [fig1, fig2, fig3, fig4]

x = np.arange(0,3383.768,.001)
i=1


for channel in range(len(ICA)):
    ICA[channel] = pd.DataFrame(ICA[channel]).clip(-1000,1000)[0]

print ICA[0][0]

for index, column in enumerate(ICA):
    #band = band +
    if(i<=16):
        ax = fig1.add_subplot(4, 4, i)
        ax.set_title('Channel %s' % (i))
        ax.set_xlabel('Sec')
        ax.plot(x, column)
    elif(i<=32):
        ax = fig2.add_subplot(4, 4, i-16)
        ax.set_title('Channel %s' % (i))
        ax.set_xlabel('Sec')
        ax.plot(x, column)
        print i
    elif (i <= 48):
        ax = fig3.add_subplot(4, 4, i - 32)
        ax.set_title('Channel %s' % (i))
        ax.set_xlabel('Sec')
        ax.plot(x, column)
        print i
    elif (i <= 64):
        ax = fig4.add_subplot(4, 4, i - 48)
        ax.set_title('Channel %s' % (i))
        ax.set_xlabel('Sec')
        ax.plot(x, column)
        print i
    i+=1

plt.show()