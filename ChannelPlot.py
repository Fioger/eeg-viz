import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sci
import json as simplejson
import scipy.signal as signal
from scipy.stats import pearsonr

mat = sci.loadmat('edf_ICA_channels.mat')
data = mat['arr']

fig1 = plt.figure(111)
"""
temp = [[None for j in range(len(data[0]))] for i in range(len(data))]

print "start"

for i in range(1):
    print i
    for j in range(len(data[0])):
        temp[i][j] = data[i][j]

#fi =data.tolist()[:10]

print "Well?"

with open('media/json/ica_data.json', 'w+') as f:
    f.write(simplejson.dumps({'data': temp}))

#data[0] = signal.detrend(data[0])
#data[1] = signal.detrend(data[1])

"""

#graph channel data
for i in range(2,61):
    for j in range(17,61):
        for time in range(112):
            time_start = time * 30000
            time_end = time_start + 30000

            if (time_start % 60000 == 0):
                time_frame = str(time_start / 60000) + ":00 - " + str(time_start / 60000) + ":30"
            else:
                time_frame = str(time_start / 60000) + ":30 - " + str(time_start / 60000 + 1) + ":00"

            ax = fig1.add_subplot(1, 1, 1)
            ax.set_title('Time' + str(time_frame)  + ' Channel '+ str(i+1) + ' vs ' + str(j+1) + ' ICA Scatter')
            #ax.set_xlabel('Sec')
            print 'Time ' + str(time_frame)  + ' Channel '+ str(i+1) + ' vs ' + str(j+1) + ' ICA Scatter'
            #print pearsonr(data[i][time*30000:(time+1)*30000],data[j][time*30000:(time+1)*30000])
            ax.scatter(data[i][time*30000:(time+1)*30000], data[j][time*30000:(time+1)*30000], color='blue', label="Fz", alpha=0.1)
            fig1.savefig('media/scatterplot/' + str(i+1) + 'v' + str(j+1) + "_" + str(time_frame)  +'.png')


"""
ax.set_xlim(10,20)
ax.set_ylim(-.00006,.00006)

axb = fig1.add_subplot(1, 1, 1)
#axb.set_title('Time 280 to 290 Seconds Channel 3(F3)')
axb.set_xlabel('Sec')
print len(x), len(data[0][270000:300000])
axb.plot(x, data[2][270000:300000], color='red', label="F3", alpha=0.5)
axb.set_xlim(10,20)
ax.set_ylim(-100,100)


for val in data[0][420000:450000]:
    if val>0:
        high3.append(val)
    else:
        low3.append(val)

for val in data[30][420000:450000]:
    if val>0:
        high4.append(val)
    else:
        low4.append(val)

ax2 = fig2.add_subplot(1, 1, 1)
ax2.set_title('Time 420 to 450 Seconds Channel 1(FP1) vs 31(FP2) ICA Scatter')
ax2.set_xlabel('Sec')
print len(x), len(data[0][420000:450000])
print pearsonr(data[0][420000:450000],data[30][420000:450000])
ax2.scatter(data[0][420000:450000], data[30][420000:450000], color='blue', label="FP1", alpha=0.1)
ax2.set_xlim(-.00015,.00015)
ax2.set_ylim(-.0001,.0001)

ax2.set_xlim(10,20)
ax2.set_ylim(-.00006,.00006)

ax2b = fig2.add_subplot(1, 1, 1)
#ax2b.set_title('Time 430 to 440 Seconds Channel 31(FP2)')
ax2b.set_xlabel('Sec')
print len(x), len(data[0][420000:450000])
ax2b.plot(x, data[30][420000:450000], color='red', label="FP2", alpha=0.5)
ax2b.set_xlim(10,20)
ax2b.set_ylim(-100,100)

for val in data[7][750000:780000]:
    if val>0:
        high5.append(val)
    else:
        low5.append(val)

for val in data[40][750000:780000]:
    if val>0:
        high6.append(val)
    else:
        low6.append(val)


ax3 = fig3.add_subplot(1, 1, 1)
ax3.set_title('Time 750 to 780 Seconds Channel 8(C3) vs 41(C5) ICA Scatter')
ax3.set_xlabel('Sec')
print len(x), len(data[0][750000:780000])
print pearsonr(data[7][750000:780000],data[40][750000:780000])
ax3.scatter(data[7][750000:780000],data[40][750000:780000], color='blue', label="C3", alpha=0.1)
ax3.set_xlim(-.00015,.00015)
ax3.set_ylim(-.0001,.0001)

ax3.set_xlim(10,20)
ax3.set_ylim(-.00006,.00006)

ax3b = fig3.add_subplot(1, 1, 1)
#ax3b.set_title('Time 760 to 770 Seconds Channel 41(C5)')
ax3b.set_xlabel('Sec')
print len(x), len(data[0][750000:780000])
ax3b.plot(x, data[40][750000:780000], color='red', label="C5", alpha=0.5)
ax3b.set_xlim(10,20)
ax3b.set_ylim(-100,100)

for val in data[2][900000:930000]:
    if val>0:
        high7.append(val)
    else:
        low7.append(val)

for val in data[33][900000:930000]:
    if val>0:
        high8.append(val)
    else:
        low8.append(val)


ax4 = fig4.add_subplot(1, 1, 1)
ax4.set_title('Time 900 to 930 Seconds Channel 3(F3) vs 34(AFz) ICA Scatter')
ax4.set_xlabel('Sec')
print len(x), len(data[0][900000:930000])
print pearsonr(data[2][900000:930000],data[33][900000:930000])
ax4.scatter(data[2][900000:930000], data[33][900000:930000], color='blue', label="F3", alpha=0.1)
ax4.set_xlim(-.008,.008)
ax4.set_ylim(-.0001,.0001)

ax4.set_xlim(10,20)
ax4.set_ylim(-.00006,.00006)


ax4b = fig4.add_subplot(1, 1, 1)
#ax4b.set_title('Time 910 to 920 Seconds Channel 34(AFz)')
ax4b.set_xlabel('Sec')
print len(x), len(data[0][900000:930000])
ax4b.plot(x, data[33][900000:930000], color='red', label="AFz", alpha=0.5)
ax4b.set_xlim(10,20)
ax4b.set_ylim(-100,100)

for val in data[14][1080000:1110000]:
    if val>0:
        high9.append(val)
    else:
        low9.append(val)

for val in data[46][1080000:1110000]:
    if val>0:
        high10.append(val)
    else:
        low10.append(val)


ax5 = fig5.add_subplot(1, 1, 1)
ax5.set_title('Time 1080 to 1110 Seconds Channel 15(O1) vs 47(PO3) ICA Scatter')
ax5.set_xlabel('Sec')
print len(x), len(data[0][1080000:1110000])
print pearsonr(data[14][1080000:1110000],data[46][1080000:1110000])
ax5.scatter(data[14][1080000:1110000], data[46][1080000:1110000], color='blue', label="O1", alpha=0.1)
ax5.set_xlim(-.00015,.00015)
ax5.set_ylim(-.0001,.0001)

ax5.set_xlim(10,20)
ax5.set_ylim(-.00006,.00006)


ax5b = fig5.add_subplot(1, 1, 1)
#ax5b.set_title('Time 1090 to 1100 Seconds Channel 47(PO3)')
ax5b.set_xlabel('Sec')
print len(x), len(data[0][1080000:1110000])
ax5b.plot(x, data[46][1080000:1110000], color='red', label="PO3", alpha=0.5)
ax5b.set_xlim(10,20)
ax5b.set_ylim(-100,100)

for val in data[11][1230000:1260000]:
    if val>0:
        high11.append(val)
    else:
        low11.append(val)

for val in data[47][1230000:1260000]:
    if val>0:
        high12.append(val)
    else:
        low12.append(val)


ax6 = fig6.add_subplot(1, 1, 1)
ax6.set_title('Time 1230 to 1260 Seconds Channel 12(Pz) vs 48(PO2) ICA Scatter')
ax6.set_xlabel('Sec')
print len(x), len(data[0][1230000:1260000])
print pearsonr(data[11][1230000:1260000],data[47][1230000:1260000])
ax6.scatter(data[11][1230000:1260000],data[47][1230000:1260000], color='blue', label="Pz", alpha=0.1)
ax6.set_xlim(-.00015,.00015)
ax6.set_ylim(-.0001,.0001)


ax6b = fig6.add_subplot(1, 1, 1)
#ax6b.set_title('Time 1240 to 1250 Seconds Channel 48(PO2)')
ax6b.set_xlabel('Sec')
print len(x), len(data[0][1230000:1260000])
ax6b.plot(x, data[47][1230000:1260000], color='red', label="PO2", alpha=0.5)
ax6b.set_xlim(10,20)
ax6b.set_ylim(-100,100)

for val in data[6][1410000:1440000]:
    if val>0:
        high13.append(val)
    else:
        low13.append(val)

for val in data[22][1410000:1440000]:
    if val>0:
        high14.append(val)
    else:
        low14.append(val)


ax7 = fig7.add_subplot(1, 1, 1)
ax7.set_title('Time 1410 to 1440 Seconds Channel 7(FC1) vs 23(Cz) ICA Scatter')
ax7.set_xlabel('Sec')
print len(x), len(data[0][1410000:1440000])
print pearsonr(data[6][1410000:1440000],data[22][1410000:1440000])
ax7.scatter(data[6][1410000:1440000], data[22][1410000:1440000], color='blue', label="FC1", alpha=0.1)
ax7.set_xlim(-.00015,.00015)
ax7.set_ylim(-.0001,.0001)

ax7.set_xlim(10,20)
ax7.set_ylim(-.00006,.00006)


ax7b = fig7.add_subplot(1, 1, 1)
#ax7b.set_title('Time 1420 to 1430 Seconds Channel 23(Cz)')
ax7b.set_xlabel('Sec')
print len(x), len(data[0][1410000:1440000])
ax7b.plot(x, data[22][1410000:1440000], color='red', label="Cz", alpha=0.5)
ax7b.set_xlim(10,20)
ax7b.set_ylim(-100,100)
plt.legend()


#find pearson data of data
start = 0
end = 0
length_of_intervals=30000
interval = 0
pearson_data = []
print (len(data[0])/length_of_intervals)+1

while end <= len(data[0]):
    print interval
    pearson_data.append(pearsonr(data[0][start:end], data[1][start:end])[0])
    start = end
    end += length_of_intervals
    interval += 1

x = np.arange(0,((len(data[0])/length_of_intervals)+1)*30,30)
ax = fig1.add_subplot(111)
ax.plot(x,pearson_data)
plt.xlabel('Seconds')
plt.ylabel('Correlation')
plt.title('ICA 37 Channel Pearson 15 vs 47')
"""

plt.show()